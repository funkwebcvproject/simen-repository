<?php
class Info_model extends CI_Model {
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}
		
		public function get_info($id = 0)
		{
			if($id === 0)
			{
				$query = $this->db->get('users');
				return $query->result_array();
			}	
			$query = $this->db->get_where('users', array('id' => $id));
			return $query->row_array();
		}
		
		public function set_info($id = 0)
		{
			$this->load->helper('url');
			
			$data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'address' => $this->input->post('address'),
					'gender' => $this->input->post('gender')
					);
			
			if ($id === 0)
			{	
				return $this->db->insert('users', $data);
			}
			else
			{
				$this->db->where('id', $id);
				$this->db->update('users', $data);
			}
		}
		
		public function delete_info($id)
		{
			$this->db->where('id', $id);
			return $this->db->delete('users');
		}
}