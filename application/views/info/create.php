<?php echo validation_errors(); ?>

<?php echo form_open ('info/create'); ?>

	<label for="firstname">First name</label>
	<input type="input" name="firstname" /><br />
	
	<label for="lastname">Last name</label>
	<input type="input" name="lastname" /><br />
	
	<label for="email">E-Mail</label>
	<input type="input" name="email" /><br />
	
	<label for="address">Address</label>
	<input type="input" name="address" /><br />
	
	<input type="radio" name="gender" value="male"> Male<br />
	<input type="radio" name="gender" value="female"> Female<br />
	<input type="radio" name="gender" value="other"> Other<br />
	
	<input type="submit" name="submit" Value="Create new person" />
	
</form>