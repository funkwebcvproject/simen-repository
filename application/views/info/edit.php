<?php echo validation_errors(); ?>

<?php echo form_open ('info/edit/'.$id); ?>

	<label for="firstname">First name</label>
	<input type="input" name="firstname" value="<?php echo $row_info['firstname']?>" /><br />
	
	<label for="lastname">Last name</label>
	<input type="input" name="lastname" value="<?php echo $row_info['lastname']?>" /><br />
	
	<label for="email">E-Mail</label>
	<input type="input" name="email" value="<?php echo $row_info['email']?>" /><br />
	
	<label for="address">Address</label>
	<input type="input" name="address" value="<?php echo $row_info['address']?>" /><br />
	
	<input type="radio" name="gender" value="male"> Male<br />
	<input type="radio" name="gender" value="female"> Female<br />
	<input type="radio" name="gender" value="other"> Other<br />
	
	<input type="submit" name="submit" Value="Update person" />
	
</form>