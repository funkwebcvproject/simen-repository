<p><a href="<?php echo site_url('info/create'); ?>">Create</a></p>

<ul>
	<?php foreach ($table_info as $row):?>
	
		<li><?php 
		
		echo 
		$row['firstname'] . " " . $row['lastname'] .
		" | E-mail: " . $row['email'] . 
		" | Last edited: " . $row['reg_date'];
		
		?></li>
		<p><a href="<?php echo site_url('info/view/'.$row['id']);?>">View info</a>
		| 
		<a href="<?php echo site_url('info/edit/'.$row['id']);?>">Edit</a>
		|
		<a href="<?php echo site_url('info/delete/'.$row['id']); ?>" onclick="return confirm('Are you sure you want to delete?')">Delete</a></p>
		
	<?php endforeach;?>

</ul>