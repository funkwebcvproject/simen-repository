<?php 
class Info extends CI_Controller {
	
		public function __construct()
		{
				parent::__construct();
				$this->load->helper('url_helper');
				$this->load->helper('form');
				$this->load->library('form_validation');		
				$this->load->model('info_model');
		}
	
		public function index()
		{
			$data['title'] = 'Index';
			$data['table_info'] = $this->info_model->get_info();
			
			$this->load->view('templates/header', $data);
			$this->load->view('info/index', $data);
			$this->load->view('templates/footer');
		}
		
		public function view()
		{
			$this->load->helper('url');
			$id = $this->uri->segment(3);
			
			if (empty($id))
			{
				show_404();
			}
			
			$data['row_info'] = $this->info_model->get_info($id);
			$data['title'] = $data['row_info']['firstname']. " ". $data['row_info']['lastname'];
			$data['id'] = $id;
			
			$this->load->view('templates/header', $data);
			$this->load->view('info/view', $data);
			$this->load->view('templates/footer');
		}
		
		public function create()
		{
			$data['title'] = 'Create new personal data';
			
			$this->form_validation->set_rules('firstname', 'First name', 'required');
			$this->form_validation->set_rules('lastname', 'Last name', 'required');
			$this->form_validation->set_rules('email', 'E-mail', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			
			if ($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header', $data);
				$this->load->view('info/create');
				$this->load->view('templates/footer');
			}
			else
			{
				$this->info_model->set_info();
				$this->load->view('info/success');
			}
		}
		
		public function edit()
		{
			$id = $this->uri->segment(3);
			
			if (empty($id))
			{
				show_404();
			}
			
			$data['title'] = 'Edit user data';
			$data['row_info'] = $this->info_model->get_info($id);
			$data['id'] = $id;
			
			$this->form_validation->set_rules('firstname', 'First name', 'required');
			$this->form_validation->set_rules('lastname', 'Last name', 'required');
			$this->form_validation->set_rules('email', 'E-mail', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			
			if ($this->form_validation->run() === FALSE)
			{
				$this->load->view('templates/header', $data);
				$this->load->view('info/edit', $data);
				$this->load->view('templates/footer');
			}
			else
			{
				$this->info_model->set_info($id);
				$this->load->view('info/success');
			}
		}
		
		public function delete()
		{
			$id = $this->uri->segment(3);
			
			if (empty($id))
			{
				show_404();
			}
			
			$user_info = $this->info_model->get_info($id);
			
			$this->info_model->delete_info($id);
			$this->load->view('info/success');
		}
}